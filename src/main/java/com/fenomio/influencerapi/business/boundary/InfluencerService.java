package com.fenomio.influencerapi.business.boundary;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fenomio.influencerapi.business.controller.InfluencerDataReader;
import com.fenomio.influencerapi.business.controller.InfluencerRepository;
import com.fenomio.influencerapi.business.entity.Influencer;
import com.restfb.types.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Optional;

@Stateless
public class InfluencerService {

    @Inject
    private InfluencerRepository influencerRepository;

    @Inject
    private InfluencerDataReader influencerDataReader;

    public Influencer getInfluencerDetails(String username) {
        Optional<Influencer> optional = influencerRepository.findByUsername(username);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            try {
                Influencer influencer = saveNewInfluencer(username);
                return influencer;
            } catch (JsonProcessingException e) {
                return null;
            }
        }
    }

    private Influencer saveNewInfluencer(String username) throws JsonProcessingException {
        Influencer influencer = new Influencer();
        influencer.setUsername(username);
        String userDetail = new ObjectMapper().writeValueAsString(influencerDataReader.readInfluencerDetails(username));
        influencer.setRaw_details(userDetail);
        influencerRepository.save(influencer);
        return influencer;
    }

}
