package com.fenomio.influencerapi.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class InfluencerApp extends Application {
}