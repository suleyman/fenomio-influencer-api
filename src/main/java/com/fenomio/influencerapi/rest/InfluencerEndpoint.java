package com.fenomio.influencerapi.rest;

import com.fenomio.influencerapi.business.boundary.InfluencerService;
import com.fenomio.influencerapi.business.entity.Influencer;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/influencer")
public class InfluencerEndpoint {

    @Inject
    private InfluencerService influencerService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInfluencer(@QueryParam("username") @NotNull String username) {
        Influencer details = influencerService.getInfluencerDetails(username);
        return Response.ok(details.getRaw_details()).build();
    }

}
